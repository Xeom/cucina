ifeq ($(LINKTYPE),EXE)
$(TARGET): $(OFILES)
	$(o-to-exe)
endif

ifeq ($(LINKTYPE),SO)
$(TARGET): $(OFILES)
	$(o-to-so)
endif

ifeq ($(LINKTYPE),STATIC)
$(TARGET): $(OFILES)
	$(o-to-a)
endif

ifeq ($(LINKTYPE),CMD)
$(TARGET):
	$(CMD) $(ERRPIPE)
endif
