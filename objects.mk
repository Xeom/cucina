ifeq ($(OBJTYPE),NORMAL)
    OFILES=$(call map,c-to-o-name,$(CFILES))
endif

ifeq ($(OBJTYPE),PIC)
    OFILES=$(call map,c-to-pico-name,$(CFILES))
endif

$(call c-to-o-name,%.c): %.c $(call c-to-d-name,%.c)
	$(c-to-o)

$(call c-to-pico-name,%.c): %.c $(call c-to-d-name,%.c)
	$(c-to-pico)

.PHONY: objs
objs: $(OFILES)

