PRINTF := printf

SUCC_COLOUR  := \e[32m
INFO_COLOUR  := \e[33m
ERR_COLOUR   := \e[31m
RESET_COLOUR := \e[0m

SUCC_NAME := *
INFO_NAME := i
ERR_NAME  := !

log=printf "$($(1)_COLOUR)[$($(1)_NAME)] $(RESET_COLOUR)$(shell printf $(2))\n"
log-print=$(info $(shell $(call log,$(1),$(2))))
