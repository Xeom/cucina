AT ?= @

define mk-target-dir
    $(AT) if [ ! -d $(@D) ]; then \
        mkdir -p $(@D); \
        $(call log,SUCC,"Made directory '%s'" $(@D)); \
    fi
endef

map=$(foreach i,$(2),$(call $(1),$(i)))
pwd=$(shell pwd)
