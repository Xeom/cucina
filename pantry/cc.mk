define c-to-o =
    $(mk-target-dir)
    $(AT)$(CC) -c $(CFLAGS) $< -o $@ $(ERRPIPE)
    $(AT)$(call log,SUCC,"Compiled '%s'" $@)
endef

define c-to-pico =
    $(mk-target-dir)
    $(AT)$(CC) -fPIC -c $(CFLAGS) $< -o $@ $(ERRPIPE)
    $(AT)$(call log,SUCC,"Compiled PIC '%s'" $@)
endef

define c-to-d =
    $(mk-target-dir)
    $(AT)$(CC) $(CFLAGS) -MM -MG -MT $(call c-to-d-name,$<) -MF $@ $< $(ERRPIPE)
    $(AT)$(call log,SUCC,"Generated dep file '%s'" $@)
endef

define o-to-so =
    $(mk-target-dir)
    $(AT)$(CC) $(CFLAGS) -shared $^ -o $@ $(LDLIBS) $(LFLAGS) $(ERRPIPE)
    $(AT)$(call log,SUCC,"Linked shared object '%s'" $@)
endef

define o-to-a =
    $(mk-target-dir)
    $(AT)$(AR) rcs $@ $^
    $(AT)$(call log,SUCC,"Linked static library '%s'" $@)
endef

define o-to-exe =
    $(mk-target-dir)
    $(AT)$(CC) $(CFLAGS) $^ -o $@ $(LDLIBS) $(LFLAGS) $(ERRPIPE)
    $(AT)$(call log,SUCC,"Linked executable '%s'" $@)
endef

