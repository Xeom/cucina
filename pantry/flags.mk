CFLAGS+=$(INC:%=-I%)
CFLAGS+=$(SYSTEM_INC:%=-isystem %)
CFLAGS+=-fdiagnostics-color=always
CFLAGS+=$(WARNINGS:%=-W%)
CFLAGS+=$(EXTRACFLAGS)
