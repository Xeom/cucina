ERR_FILE ?= errs.txt

ifeq ($(DISPLAY_ERRS),1)
    ERRPIPE= 2>&1 2>>$(ERR_FILE) || (less -R $(ERR_FILE) && rm $(ERR_FILE) && false)
    define display-errs
        if [ -s $(ERR_FILE) ]; then \
            less -R $(ERR_FILE); \
            rm -f $(ERR_FILE); \
            exit 1; \
        elif [ -f $(ERR_FILE) ]; then \
            rm -f $(ERR_FILE); \
        fi
    endef
else
    ERRPIPE=
    define display-errs
    endef
endif
