ifeq ($(DEPTYPE),NORMAL)
    DFILES=$(call map,c-to-d-name,$(CFILES))
endif

$(call c-to-d-name,%.c): %.c
	$(c-to-d)

ifeq (,$(findstring print_target,$(MAKECMDGOALS)))
    $(call log-print,INFO,"Including deps for %s" $(TARGET))
    include $(DFILES)
endif
