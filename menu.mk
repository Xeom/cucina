ifndef CUCINADIR
$(error The variable CUCINADIR must be defined)
endif

include $(CUCINADIR)pantry/*.mk

CWD=$(call pwd)

ifndef DISHES
$(call log-print,ERR,"The variable 'DISHES' must be defined.")
$(call log-print,ERR,"This should contain the names of the dishes that are")
$(call log-print,ERR,"on the menu.")
$(error The variable DISHES must be defined)
endif

define dish-make =
$(MAKE) -C$(CWD) -f $(CUCINADIR)dish.mk CUCINADIR=$(CUCINADIR) $(DISH_$(1)_PARAMS) $(DISH_GLOBAL_PARAMS)
endef

define create-dish =
$(call log-print,INFO,"___ Cooking dish %s (%s) ___" $(1) $(shell $(call dish-make,$(1)) -s print_target))
$(call dish-make,$(1)) target

endef


define dish-info =
$(call log,INFO,"DISH_%s:" $(1))
$(call log,INFO,"    Target: %s" $(shell $(call dish-make,$(1)) -s print_target))
$(call log,INFO,"    Parameters:")
$(call log,INFO,"      * %%s") $(DISH_$(1)_PARAMS)

endef

define dish-help =
$(call log,INFO,"%16s - Build %s." DISH_$(1) $(1))

endef

DISH_%_TARGET_NAME:
	$(AT)$(call dish-make,$*) -s print_target

DISH_%:
	$(AT)$(call create-dish,$*)

list:
	@$(call log,INFO,"Global parameters:")
	@$(call log,INFO,"      * %%s") $(DISH_GLOBAL_PARAMS)
	@$(call map,dish-info,$(DISHES))

help:
	@$(call log,INFO,"Help")
	@$(call log,INFO,"")
	@$(call log,INFO,"Availiable targets:")
	@$(call log,INFO,"%16s - %s" "help" "Show this message.")
	@$(call log,INFO,"%16s - %s" "all"  "Compile all dishes.")
	@$(call log-msg,INFO,"%16s - %s" "list" "List information about dishes.")
	@$(call map,dish-help,$(DISHES))

all: $(addprefix DISH_,$(DISHES))

.DEFAULT_GOAL := help
.PHONY: list_menu all DISH_%
