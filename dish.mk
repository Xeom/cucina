TYPE ?= STATIC_LIB

MAJOR ?= 1
MINOR ?= 0
PATCH ?= 0

VERSION = $(MAJOR).$(MINOR).$(PATCH)

NAME     ?= out
BUILDDIR ?= build/

OBJDIR ?= $(BUILDDIR)obj/
DEPDIR ?= $(BUILDDIR)dep/
BINDIR ?= $(BUILDDIR)bin/

DISPLAY_ERRS ?= 1

ifeq ($(VERBOSE),1)
    AT =
else
    AT = @
endif

KNOWNTYPE = no

ifeq ($(TYPE),EXECUTABLE)
    TARGET   ?= $(BINDIR)$(NAME)-$(VERSION)
    OBJTYPE  ?= NORMAL
    LINKTYPE ?= EXE
    DEPTYPE  ?= NORMAL
    KNOWNTYPE = yes
endif

ifeq ($(TYPE),SHARED_OBJ)
    TARGET   ?= $(BINDIR)lib$(NAME).so.$(VERSION)
    OBJTYPE  ?= PIC
    LINKTYPE ?= SO
    DEPTYPE  ?= NORMAL
    KNOWNTYPE = yes
endif

ifeq ($(TYPE),STATIC_LIB)
    TARGET   ?= $(BINDIR)lib$(NAME)-$(VERSION).a
    OBJTYPE  ?= NORMAL
    LINKTYPE ?= STATIC
    DEPTYPE  ?= NORMAL
    KNOWNTYPE = yes
endif

ifeq ($(TYPE),CMD)
    TARGET   ?= $(NAME)-$(VERSION)
    OBJTYPE  ?= NONE
    LINKTYPE ?= CMD
    DEPTYPE  ?= NONE
    KNOWNTYPE = yes

.PHONY: $(TARGET)

endif

ifneq ($(KNOWNTYPE),yes)
    $(call log,ERR,"Unknown TYPE: %s" $(TYPE))
endif

include $(CUCINADIR)pantry/*.mk

include $(CUCINADIR)deps.mk
include $(CUCINADIR)objects.mk
include $(CUCINADIR)link.mk

.FORCE:
.PHONY: .FORCE

.PHONY: print_target_info
print_target_info: .FORCE
	$(AT)$(call log,INFO,"Creating target %s" $(TARGET))
	$(AT)$(call log,INFO," *     Version: %s" $(VERSION))
	$(AT)$(call log,INFO," *        Type: %s" $(TYPE))

.PHONY: target
target: print_target_info $(TARGET) .FORCE
	$(AT)$(display-errs)

.PHONY: print_target
print_target: .FORCE
	$(AT)echo $(TARGET)

.DEFAULT_GOAL=target

